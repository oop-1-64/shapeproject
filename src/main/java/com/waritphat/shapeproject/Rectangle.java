/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.shapeproject;

/**
 *
 * @author domem
 */
public class Rectangle {
    private double h;
    private double w;
    public Rectangle (double h,double w){
        this.h = h;
        this.w = w;
    }
    public double calArea(){
        return h*w;
    }
    public void setH(double h){
        if (h <= 0){
            System.out.println("Error: Height must more than zero");
            return;
        }   
        this.h = h;
    }
    public void setW(double w){
        if (w <= 0){
            System.out.println("Error: Width must more than zero");
            return;
        }   
        this.w = w;
    }
    public double getH(){
        return h;
    }
    public double getW(){
        return w;
    }
    @Override
    public String toString(){
        return "Area of rectangle1 (h = "+ this.getH()+ ",b = "+ this.getW()+" ) is " + this.calArea();
    }
    
    
    
}
