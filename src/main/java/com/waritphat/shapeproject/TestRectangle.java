/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.shapeproject;

/**
 *
 * @author domem
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(3,5);
        System.out.println("Area of rectangle1 (h = "+ rectangle1.getH()+ ",b = "+ rectangle1.getW()+" ) is "+ rectangle1.calArea());
        rectangle1.setH(0);
        rectangle1.setW(5);
        System.out.println(rectangle1.toString());
        rectangle1.setH(3);
        rectangle1.setW(0);
        System.out.println(rectangle1.toString());
    }
}
