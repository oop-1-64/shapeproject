/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.shapeproject;

/**
 *
 * @author domem
 */
public class Triangle {
    double h;
    double b;
    public static final double oh = 0.5;
    public Triangle (double h, double b){
        this.h = h;
        this.b = b;
    }
    public double calArea(){
        return oh*h*b;
    }
    public void setH(double h){
        if (h<=0){
            System.out.println("Error: Height must more than zero");
            return;
        }
        this.h = h;
    }
    public void setB(double b){
        if (b<=0){
            System.out.println("Error: Base must more than zero");
            return;
        }
        this.b = b;
    }
    public double getH(){
        return h;
    }
    public double getB(){
        return b;
    }
    @Override
    public String toString(){
        return "Area of triangle1 (h = "+ this.getH()+ ","+ this.getB()+" ) is " + this.calArea();
    }
}
